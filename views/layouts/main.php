<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <?= Html::csrfMetaTags() ?> 
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'Departamento de Alumnos',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            if (Yii::$app->user->can('administrador')) {
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    //['label' => 'Horarios', 'url' => ['/aluhorariodetalle/index']],
                    //['label' => 'Exámenes', 'url' => ['/aluexamen/index']],
                    ['label' => 'Admin Novedades', 'url' => ['/alunovedades/index']],
                    ['label' => 'Admin Llamados', 'url' => ['/alumesa/index']],
                    ['label' => 'Admin Horarios', 'url' => ['/aluhorario/index']],
                    ['label' => 'Admin Aulas', 'url' => ['/aluaula/index']],                                                         
                    ['label' => 'Admin Exámenes', 'url' => ['/aluexamen/admin','carrera'=>'1']],                                                                             
                    Yii::$app->user->isGuest ?
                        ['label' => 'Login', 'url' => ['/site/login']] :
                        ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']],
                ],
            ]);
            }
            else {
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Horarios', 'url' => ['/aluhorariodetalle/index']],
                    ['label' => 'Exámenes', 'url' => ['/aluexamen/index']],
                    ['label' => 'Novedades', 'url' => ['/alunovedades/index']],
                    ['label' => 'Ocupación de Instalaciones', 'url' => ['/aluaula/index']],                                     
                    Yii::$app->user->isGuest ?
                        ['label' => 'Login', 'url' => ['/site/login']] :
                        ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']],
                ],
            ]);                
            }
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">Facultad de Turismo - Ciclo Lectivo <?= date('Y') ?></p>
            
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<script src="http://repository.chatwee.com/scripts/6695a667e7bcdf04677bd5fc66888868.js" type="text/javascript" charset="UTF-8"></script>