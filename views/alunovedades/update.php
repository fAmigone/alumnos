<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Alunovedades $model
 */

$this->title = 'Update Alunovedades: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Alunovedades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alunovedades-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
