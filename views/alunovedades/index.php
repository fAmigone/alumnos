    <?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\AlunovedadesSearch $searchModel
 */

$this->title = 'Novedades';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alunovedades-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
    <?php 
        if (Yii::$app->user->can('administrador')) {
             echo Html::a('Crear Novedades', ['create'], ['class' => 'btn btn-success']);
        }
    ?>
</p>
    
        


      <?= ListView::widget([
    	'dataProvider' => $dataProvider,
        'itemView'=>'view',
    ]); ?>


</div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&appId=223584634455878&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


