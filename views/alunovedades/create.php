<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Alunovedades $model
 */

$this->title = 'Crear Novedad';
$this->params['breadcrumbs'][] = ['label' => 'Novedades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alunovedades-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
