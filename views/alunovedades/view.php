<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
/**
 * @var yii\web\View $this
 * @var app\models\Alunovedades $model
 */  

$this->title = 'Novedades';
//$this->params['breadcrumbs'][] = ['label' => 'Novedades', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>


<div class="col-sm-6 col-md-4">
   <div class="thumbnail" style="background-color:#E0ECF8;">       
        <center><span class="glyphicon glyphicon-flag"></span></center>
        <div class="caption">        
            <?php echo '<p>'.$model->novedad.'</p>'; ?>     
            <hr style="color: #000000;">
            <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-type="button"></div>       
        
        <p>
            
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
      </div>      
   </div>
</div>  




