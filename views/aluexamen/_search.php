<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/**
 * @var yii\web\View $this
 * @var app\models\AluexamenSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="aluexamen-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

   

   <?php // echo $form->field($model, 'idmesa')->dropDownList(ArrayHelper::map(\app\models\Alumesa::find()->all(),'id','nombre'))?>    

     <?php
    
    $dataCategory=ArrayHelper::map(\app\models\Alucarrera::find()->asArray()->all(), 'id', 'nombre');
    
    echo $form->field($model, 'carrera')->dropDownList($dataCategory, 
             ['prompt'=>'-Selecciona la Carrera-',
              'onchange'=>'
                $.post( "'.Yii::$app->urlManager->createUrl('aluhorario/lists?id=').'"+$(this).val(), function( data ) {
                  $( "select#id" ).html(data);
                });
            ']); 
 
        $dataPost=ArrayHelper::map(\app\models\Aluhorario::find()->asArray()->all(), 'id', 'idmateria');
    echo $form->field($model, 'idmateria')
        ->dropDownList(            
            $dataPost,           
            ['id'=>'id',
             'prompt'=>'-Selecciona la Materia-',   ]
        ); 
    ?>

       
       <?php 
           //echo  $form->field($model, 'carrera')->dropDownList(ArrayHelper::map(\app\models\Alucarrera::find()->all(),'id','nombre'));

         //   echo $form->field($model, 'idmateria')->dropDownList(ArrayHelper::map(\app\models\Aluhorario::find()->all(),'id','idmateria'));

   ?>    

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
