<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/**
 * @var yii\web\View $this
 * @var app\models\AluexamenSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="aluexamen-search">

    <?php $form = ActiveForm::begin([
        'action' => ['admin'],
        'method' => 'get',
    ]); ?>

   

   <?= $form->field($model, 'idmesa')->dropDownList(ArrayHelper::map(\app\models\Alumesa::find()->all(),'id','nombre'))?>    

   <?= $form->field($model, 'carrera')->dropDownList(ArrayHelper::map(\app\models\Alucarrera::find()->all(),'id','nombre'))?>    

    <div class="form-group">
        
            <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
