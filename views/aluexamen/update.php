<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Aluexamen $model
 */

$this->title = 'Actualizar Fechas de Examen: '; 
$this->params['breadcrumbs'][] = ['label' => 'Administrar Examenes', 'url' => ['admin']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="aluexamen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
