<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use kartik\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
/**
 * @var yii\web\View $this
 * @var app\models\Aluexamen $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="aluexamen-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'idmesa')->dropDownList(ArrayHelper::map(\app\models\Alumesa::find()->all(),'id','nombre'))?>    
    
        
    <?php
    
    $dataCategory=ArrayHelper::map(\app\models\Alucarrera::find()->asArray()->all(), 'id', 'nombre');
    
    echo $form->field($model, 'carrera')->dropDownList($dataCategory, 
             ['prompt'=>'-Selecciona la Carrera-',
              'onchange'=>'
                $.post( "'.Yii::$app->urlManager->createUrl('aluhorario/lists?id=').'"+$(this).val(), function( data ) {
                  $( "select#id" ).html(data);
                });
            ']); 
 
        $dataPost=ArrayHelper::map(\app\models\Aluhorario::find()->asArray()->all(), 'id', 'idmateria');
    echo $form->field($model, 'idmateria')
        ->dropDownList(            
            $dataPost,           
            ['id'=>'id',
             'prompt'=>'-Selecciona la Materia-',   ]
        ); 
    ?>
    
    <?php //echo $form->field($model, 'idmateria')->dropDownList(ArrayHelper::map(\app\models\Aluhorario::find()->all(),'id','idmateria'))?>    
    <?= $form->field($model, 'fecha1')->widget(DateTimePicker::classname(), [
	'options' => ['placeholder' => 'Ingrese el dia y hora del examen...'],
	'pluginOptions' => [
		'autoclose' => true,
                'format' => 'dd/mm/yyyy hh:ii'
	]
]);
            ?>

    <?= $form->field($model, 'cancela1')->widget(DateTimePicker::classname(), [
	'options' => ['placeholder' => 'Ingrese el dia y hora del examen...'],
	'pluginOptions' => [
		'autoclose' => true,
                'format' => 'dd/mm/yyyy hh:ii'
	]
]); ?>

    <?= $form->field($model, 'fecha2')->widget(DateTimePicker::classname(), [
	'options' => ['placeholder' => 'Ingrese el dia y hora del examen...'],
	'pluginOptions' => [
		'autoclose' => true,
                'format' => 'dd/mm/yyyy hh:ii'
	]
]); ?>

    <?= $form->field($model, 'cancela2')->widget(DateTimePicker::classname(), [
	'options' => ['placeholder' => 'Ingrese el dia y hora del examen...'],
	'pluginOptions' => [
		'autoclose' => true,
                'format' => 'dd/mm/yyyy hh:ii'
	]
]); ?>

    
       

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>        
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
