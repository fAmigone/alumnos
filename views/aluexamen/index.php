<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\AluexamenSearch $searchModel
 */

$this->title = 'Exámenes';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aluexamen-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>


    <?php 
         if (Yii::$app->user->can('administrador')) {
         
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],


                        'idmesa0.nombre',
                        'idmateria0.idmateria',
                        'fecha1',
                        'cancela1',
                        'fecha2',
                        'cancela2',


                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); 
         }
         else 
         {
             echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],


                        'idmesa0.nombre',
                        'idmateria0.idmateria',
                        'fecha1',
                        'cancela1',
                        'fecha2',
                        'cancela2',


                    
                    ],
                ]); 
         }
    ?>
 
   
    <p>
        
        <?php 
         if (Yii::$app->user->can('administrador')) {
         echo Html::a('Crear Exámen', ['create'], ['class' => 'btn btn-success']); }?>
         
    </p>    
</div>
