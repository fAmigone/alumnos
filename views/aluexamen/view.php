<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\Aluexamen $model
 */

$this->title = $model->idmateria0->idmateria;
$this->params['breadcrumbs'][] = ['label' => 'Volver a Administrar Examenes', 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aluexamen-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>  
        <?php 
         if (Yii::$app->user->can('administrador')) {
                echo Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); 
                echo ' ';
                echo Html::a('Eliminar', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Seguro?',
                        'method' => 'post',
                    ],
         ]); }?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'idmesa0.nombre',
            'fecha1',
            'cancela1',
            'fecha2',
            'cancela2',
            'idmateria0.idmateria',
        ],
    ]) ?>

</div>
