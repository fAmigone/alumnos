<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\AluhorariodetalleSearch $searchModel
 */

$this->title = 'Horario de Cursadas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aluhorariodetalle-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    
    <?php 
        if (Yii::$app->user->can('administrador')) {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
      //  'filterModel' => $searchModel,
        'columns' => [
            
            'idaluhorario0.idanio0.nombre',
            'idaluaula0.nombre',
            'idaludiadia0.nombre',
            'inicio',
            'fin',

            ['class' => 'yii\grid\ActionColumn'],
        ],
        ]);} 
    else {
        echo GridView::widget([
        'dataProvider' => $dataProvider,
      //  'filterModel' => $searchModel,
        'columns' => [
            
            'idaluhorario0.idanio0.nombre',
            'idaluaula0.nombre',
            'idaludiadia0.nombre',
            'inicio',
            'fin',        
        ],
    ]); 
    }
    ?>

</div>
