<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Aluhorariodetalle $model
 */

$this->title = 'Horarios de Materias:';
$this->params['breadcrumbs'][] = ['label' => 'Aluhorariodetalles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="aluhorariodetalle-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
