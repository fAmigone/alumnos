<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Alumesa $model
 */

$this->title = 'Crear Llamado';
$this->params['breadcrumbs'][] = ['label' => 'Alumesas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumesa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
