<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
/**
 * @var yii\web\View $this
 * @var app\models\Aluaula $model
 */

$this->title = 'Aula '.$model->nombre;
//$this->params['breadcrumbs'][] = ['label' => 'Aluaulas', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aluaula-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
     <?php 
        if (Yii::$app->user->can('administrador')) {    
        echo Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
        echo ' ';
        echo Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]); }?>
        
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataprovider,      
        'columns' => [            
            
            'idaluhorario0.idmateria',
            'idaludiadia0.nombre',
            'inicio',
            'fin',            
        ],
        ]); 
   ?>
</div>
