<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/**
 * @var yii\web\View $this
 * @var app\models\AluaulaSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="aluaula-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    
        <?= $form->field($model, 'idaluaula')->dropDownList(ArrayHelper::map(\app\models\Aluaula::find()->all(),'id','nombre'))?>    
    
    <div class="form-group">
        <?= Html::submitButton('Ver Ocupación', ['class' => 'btn btn-primary']) ?>       
    </div>

    <?php ActiveForm::end(); ?>

</div>
