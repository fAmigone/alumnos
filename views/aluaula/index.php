    <?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\AluaulaSearch $searchModel
 */

$this->title = 'Ocupación de Instalaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aluaula-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php   if (Yii::$app->user->can('administrador')) { 
        echo Html::a('Nueva Aula', ['create'], ['class' => 'btn btn-success']); 
        }?>
    </p>

    
    <?php 
        if (Yii::$app->user->can('administrador')) { 
    echo GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [                  
            'idaludiadia0.nombre',
            'inicio',
            'fin',
            
        ],
     ]);} 
     else {
         echo GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [                  
            'idaludiadia0.nombre',
            'inicio',
            'fin',

              
        ],
     ]);}               
     ?>

</div>
