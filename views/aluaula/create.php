<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Aluaula $model
 */

$this->title = 'Nueva Aula';
$this->params['breadcrumbs'][] = ['label' => 'Aluaulas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aluaula-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
