<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Alumateria $model
 */

$this->title = 'Update Alumateria: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Alumaterias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alumateria-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
