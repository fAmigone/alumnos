<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Alumateria $model
 */

$this->title = 'Nueva Materia';
$this->params['breadcrumbs'][] = ['label' => 'Alumaterias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumateria-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
