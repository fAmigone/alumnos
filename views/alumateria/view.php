<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
/**
 * @var yii\web\View $this
 * @var app\models\Alumateria $model
 */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Alumaterias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumateria-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can('administrador')) {
        
            echo  Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); 
            echo '  ';    
            echo Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Seguro?',
                'method' => 'post',
            ],
        ]);} ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'idcarrera0.nombre',
        ],
    ]) ?>
    
    
       <?= GridView::widget([
        'dataProvider' => $dataAluhorariodetalleProvider,
        //'filterModel' => $searchAluhorariodetalleModel,
        'pager' => ['pagination'=>false],
        'columns' => [            
            'idaluaula0.nombre',
            'idaludiadia0.nombre',
            'inicio',
             'fin',
            ['class' => 'yii\grid\ActionColumn',
                 'controller'=>'aluhorariodetalle',
                ],
        ],
    ]); ?>

     <p>
        <?= Html::a('Agregar Horario', ['/aluhorariodetalle/create'], ['class' => 'btn btn-success']) ?>
    </p>
    
</div>
