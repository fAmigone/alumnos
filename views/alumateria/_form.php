<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/**
 * @var yii\web\View $this
 * @var app\models\Alumateria $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="alumateria-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => 300]) ?>
    
    <?= $form->field($model, 'idcarrera')->dropDownList(ArrayHelper::map(\app\models\Alucarrera::find()->all(),'id','nombre'))?>    
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
