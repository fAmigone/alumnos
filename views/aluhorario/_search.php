<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var app\models\AluhorarioSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="aluhorario-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

   

    
    <?php  //echo $form->field($model, 'idcarrera')->dropDownList(ArrayHelper::map(\app\models\Alucarrera::find()->all(),'id','nombre'))?>
    <?php //echo $form->field($model, 'idanio')->dropDownList(ArrayHelper::map(\app\models\Aluanio::find()->all(),'id','nombre'))?>
    
    <?php //echo $form->field($model, 'idmateria')->dropDownList(ArrayHelper::map(\app\models\Aluhorario::find()->all() ,'id','idmateria'))?>
    <?php //$form->field($model, 'idEtapa')->dropDownList(ArrayHelper::map(Etapaproyecto::find()->where('idProyecto=:idProyecto', [':idProyecto'=>$idProyecto])->all(),'idEtapa','nombre')) ?>
    <?= $form->field($model, 'idmateria')->dropDownList(ArrayHelper::map(\app\models\Aluhorario::find()->where('idcarrera=:idcarrera', [':idcarrera'=>$form->field($model, 'idcarrera')])->all() ,'id','idmateria'))?>
    


    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
