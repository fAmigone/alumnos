<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\AluhorarioSearch $searchModel
 */

$this->title = 'Materias y Horarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aluhorario-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'idcarrera0.nombre',
            'idanio0.nombre',
            'idmateria',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<p>
        <?= Html::a('Crear Materia', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
