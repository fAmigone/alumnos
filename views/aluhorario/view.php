<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
/**
 * @var yii\web\View $this
 * @var app\models\Aluhorario $model
 */

$this->title = $model->idmateria;
$this->params['breadcrumbs'][] = ['label' => 'Aluhorarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aluhorario-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        
        <?php 

        //$model = new app\models\Aluhorario;        
        //echo $this->render('_search', ['model' => $searchModel]);        
        
        if (Yii::$app->user->can('administrador')) {
            echo Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);                 
        }
        ?>
        <?php 
        if (Yii::$app->user->can('administrador')) { 
        echo Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Seguro?',
                'method' => 'post',
            ],
        ]); }?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [            
            'idcarrera0.nombre',
            'idmateria',
            'idanio0.nombre',
        ],
    ]) ?>
    
    <p>
    <h2>Horarios para la materia </h2>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataAluhorariodetalleProvider,
        //'filterModel' => $searchAluhorariodetalleModel,
        'pager' => ['pagination'=>false],
        'columns' => [            
            'idaluaula0.nombre',
            'idaludiadia0.nombre',
            'inicio',
             'fin',
            ['class' => 'yii\grid\ActionColumn',
                 'controller'=>'aluhorariodetalle',
                ],
        ],
    ]); ?>
    
    
     <?php 
      if (Yii::$app->user->can('administrador')) { 
        echo '<h2>Nuevo Horario </h2>';  
        $modelUnDetalle->idaluaula = NULL;
        $modelUnDetalle->idaludiadia = NULL;
        $modelUnDetalle->inicio = NULL;
        $modelUnDetalle->fin= NULL;
        
        echo $this->render('_formDetalle', [
        'model' => $modelUnDetalle
      ]);}
        ?>
</div>
