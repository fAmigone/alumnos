<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Aluhorario $model
 */

$this->title = 'Update Aluhorario: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Aluhorarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="aluhorario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
