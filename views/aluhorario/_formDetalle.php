<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/**
 * @var yii\web\View $this
 * @var app\models\Aluhorariodetalle $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="aluhorariodetalle-form">

    <?php $form = ActiveForm::begin(); ?>
 
    
    <?= $form->field($model, 'idaluaula')->dropDownList(ArrayHelper::map(\app\models\Aluaula::find()->all(),'id','nombre'))?>

    <?= $form->field($model, 'idaludiadia')->dropDownList(ArrayHelper::map(\app\models\Aludia::find()->all(),'id','nombre'))?>

    <?= $form->field($model, 'inicio')->textInput() ?>

    <?= $form->field($model, 'fin')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Agregar Horario' : 'Agregar Horario', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

