<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var app\models\Aluhorario $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="aluhorario-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <?= $form->field($model, 'idcarrera')->dropDownList(ArrayHelper::map(\app\models\Alucarrera::find()->all(),'id','nombre'))?>    

    
      <?= $form->field($model, 'idmateria')->textInput() ?>

    
    <?= $form->field($model, 'idanio')->dropDownList(ArrayHelper::map(\app\models\Aluanio::find()->all(),'id','nombre'))?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
