<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aluaula".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Aluhorariodetalle[] $aluhorariodetalles
 */
class Aluaula extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aluaula';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Aula',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAluhorariodetalles()
    {
        return $this->hasMany(Aluhorariodetalle::className(), ['idaluaula' => 'id']);
    }
}
