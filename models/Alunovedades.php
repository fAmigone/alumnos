<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alunovedades".
 *
 * @property integer $id
 * @property string $novedad
 */
class Alunovedades extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alunovedades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['novedad'], 'required'],
            [['novedad'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'novedad' => 'Novedad',
        ];
    }
}
