<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Aluhorariodetalle;

/**
 * AluhorariodetalleSearch represents the model behind the search form about `app\models\Aluhorariodetalle`.
 */
class AluhorariodetalleSearch extends Aluhorariodetalle
{
    public $carrera;
    public function rules()
    {
        return [
            [['id','idaluhorario', 'idaluaula', 'idaludiadia'], 'integer'],
            [['inicio', 'fin', 'carrera'], 'safe'],
        ];
    }
 public function attributeLabels()
    {
        return [
            'id' => 'Selecciona la materia', 
            'idaluaula'=> ' Seleccionar:'
        ];
    }
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    

    public function search($params)
    {
        $query = Aluhorariodetalle::find();
        $query -> joinWith('idaluhorario0');
         
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        if ($this->id == NULL) {                    
           $mat=NULL;
        }
        else {
            $unaMateria = new Aluhorario;
            $unaMateria=  $unaMateria->findOne($this->id);                
            $mat = $unaMateria->idmateria;
        }
        $carrera= $this->carrera;
        $query->andFilterWhere([
            //'aluhorariodetalle.id' => $this->id,
            'idaluhorario' => $this->idaluhorario,
            'idcarrera' => $this->carrera,
            'idaludiadia' => $this->idaludiadia,
            'inicio' => $this->inicio,
            'fin' => $this->fin,
            'idaluaula'=>$this->idaluaula,
            'idmateria'=>$mat,
        ]);

        return $dataProvider;
    }
}
