<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aluexamen".
 *
 * @property integer $id
 * @property integer $idmesa
 * @property string $fecha1
 * @property string $cancela1
 * @property string $fecha2
 * @property string $cancela2
 *
 * @property Alumesa $idmesa0
 */
class Aluexamen extends \yii\db\ActiveRecord
{
public $carrera;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aluexamen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idmesa', 'fecha1', 'cancela1', 'fecha2', 'cancela2'], 'required'],
            [['idmesa', 'idmateria'], 'integer'],
            [['fecha1', 'cancela1', 'fecha2', 'cancela2'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idmesa' => 'Llamado',
            'fecha1' => 'Primer Fecha',
            'cancela1' => 'Primer Cancelación',
            'fecha2' => 'Segunda Fecha',
            'cancela2' => 'Segunda Cancelación',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdmesa0()
    {
        return $this->hasOne(Alumesa::className(), ['id' => 'idmesa']);
    }
     public function getIdmateria0() 
   { 
       return $this->hasOne(Aluhorario::className(), ['id' => 'idmateria']); 
   }
  
}
