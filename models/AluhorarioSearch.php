<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Aluhorario;

/**
 * AluhorarioSearch represents the model behind the search form about `app\models\Aluhorario`.
 */
class AluhorarioSearch extends Aluhorario
{
    public function rules()
    {
        return [
            [['id', 'idcarrera', 'idmateria', 'idanio'], 'integer'],
                       [['idmateria'], 'safe'],

        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Aluhorario::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $unaMateria = new Aluhorario;
        $unaMateria=  $this->findOne($this->idmateria);                
        $mat = $unaMateria->idmateria;
        $query->andFilterWhere([
            'id' => $this->id,
            'idcarrera' => $this->idcarrera,
            'idmateria'=> $mat,
            'idanio' => $this->idanio,
        ]);
        //$query->andFilterWhere(['like', 'idmateria', $this->idmateria]); 
        return $dataProvider;
    }
}
