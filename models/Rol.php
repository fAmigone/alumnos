<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Rol".
 *
 * @property integer $id
 * @property string $Rol
 *
 * @property Usuario[] $usuarios
 */
class Rol extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Rol';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Rol'], 'required'],
            [['Rol'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Rol' => 'Rol',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['idRol' => 'id']);
    }
}
