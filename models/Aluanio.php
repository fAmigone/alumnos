<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aluanio".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Aluhorario[] $aluhorarios
 */
class Aluanio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aluanio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Año',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAluhorarios()
    {
        return $this->hasMany(Aluhorario::className(), ['idanio' => 'id']);
    }
}
