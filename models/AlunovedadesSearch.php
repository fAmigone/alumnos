<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Alunovedades;

/**
 * AlunovedadesSearch represents the model behind the search form about `app\models\Alunovedades`.
 */
class AlunovedadesSearch extends Alunovedades
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['novedad'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Alunovedades::find()->orderBy('id DESC') ;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,            
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'novedad', $this->novedad]);
        
        return $dataProvider;
    }
    
      public static function defaultOrder($query)
        {
              $query->orderBy('id DESC');
        }
}
