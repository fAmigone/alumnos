<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aluhorariodetalle".
 *
 * @property integer $id
 * @property integer $idaluhorario
 * @property integer $idaluaula
 * @property integer $idaludiadia
 * @property string $inicio
 * @property string $fin
 *
 * @property Aluhorario $idaluhorario0
 * @property Aluaula $idaluaula0
 * @property Aludia $idaludiadia0
 */
class Aluhorariodetalle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aluhorariodetalle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idaluhorario', 'idaluaula', 'idaludiadia', 'inicio', 'fin'], 'required'],
            [['idaluhorario', 'idaluaula', 'idaludiadia'], 'integer'],                        
            [['inicio', 'fin'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idaluhorario' => 'Horario',
            'idaluaula' => 'Aula',
            'idaludiadia' => 'Día',
            'inicio' => 'Horario Inicio',
            'fin' => 'Horario Fin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdaluhorario0()
    {
        return $this->hasOne(Aluhorario::className(), ['id' => 'idaluhorario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdaluaula0()
    {
        return $this->hasOne(Aluaula::className(), ['id' => 'idaluaula']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdaludiadia0()
    {
        return $this->hasOne(Aludia::className(), ['id' => 'idaludiadia']);
    }
     public function getcarrera()
    {
      
    }
}
