<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Aluexamen;

/**
 * AluexamenSearch represents the model behind the search form about `app\models\Aluexamen`.
 */
class AluexamenSearch extends Aluexamen
{
    
    public function rules()
    {
        return [
            [['id', 'idmesa', 'carrera','idmateria'], 'integer'],
            [['fecha1', 'cancela1', 'fecha2', 'cancela2'], 'safe'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'idmesa' => 'Llamado',
            'idmateria' => 'Materia',
        ];
    }
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Aluexamen::find();
        $query -> joinWith('idmateria0');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'id' => $this->id,
            'idmesa' => $this->idmesa,
            'aluexamen.idmateria' => $this->idmateria,
            'idcarrera' => $this->carrera,
        ]);

   

        return $dataProvider;
    }
    public function searchadmin($params)
    {
        $query = Aluexamen::find();
        $query -> joinWith('idmateria0');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        //echo 'carrera vale '.$this->carrera; exit;
        $query->andFilterWhere([
            'id' => $this->id,
            'idmesa' => $this->idmesa,
            'idmateria' => $this->idmateria,
            'idcarrera' => $this->carrera,
        ]);

   

        return $dataProvider;
    }
  
}
