<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aluhorario".
 *
 * @property integer $id
 * @property integer $idcarrera
 * @property integer $idmateria
 * @property integer $idanio
 *
 * @property Aluanio $idanio0
 * @property Alucarrera $idcarrera0
 * @property Alumateria $idmateria0
 * @property Aluhorariodetalle[] $aluhorariodetalles
 */
class Aluhorario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aluhorario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idcarrera', 'idmateria', 'idanio'], 'required'],
            [['idcarrera', 'idanio'], 'integer'],
            [['idmateria'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idcarrera' => 'Carrera',
            'idmateria' => 'Materia',
            'idanio' => 'Año',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdanio0()
    {
        return $this->hasOne(Aluanio::className(), ['id' => 'idanio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdcarrera0()
    {
        return $this->hasOne(Alucarrera::className(), ['id' => 'idcarrera']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAluhorariodetalles()
    {
        return $this->hasMany(Aluhorariodetalle::className(), ['idaluhorario' => 'id']);
    }
}
