<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aludia".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Aluhorariodetalle[] $aluhorariodetalles
 */
class Aludia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aludia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 11]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Día',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAluhorariodetalles()
    {
        return $this->hasMany(Aluhorariodetalle::className(), ['idaludiadia' => 'id']);
    }
}
