<?php
namespace app\models;
use Yii;


class UsuarioAuth  {

    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation Name of the operation required (here, a role).
     * @param mixed $params (opt) Parameters for this operation, usually the object to access.
     * @return bool Permission granted?
     */
    public function checkAccess($operation, $params = array()) {
        
        
        if (\Yii::$app->user->isGuest ) {
            // Not identified => no rights
            return false;
        }

        $role = Yii::$app->user->identity->rol;
        if ($role === 'administrador') {
            return true; // admin role has access to everything
        }
        /**
         * chequeo si es usuario asignado al proyecto
         */
//        if ($operation === 'usuarioVinculadoProyecto') {
//            /* @var Usuario $usuario */
//            if (isset($params['Proyecto'])) {
//                $proyecto = $params['Proyecto'];
//                if (isset($params['Funcion'])) {
//                    $idFuncion = $params['Funcion'];
//                } else {
//                    $idFuncion = null;
//                }
//                $idUsuario = $this->getId();
//                return $proyecto->usuarioVinculado($idUsuario, $idFuncion);
//            }
//        }
                
        // allow access if the operation request is the current user's role
        return ($params === $role);
    }

}
