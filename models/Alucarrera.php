<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alucarrera".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Aluhorario[] $aluhorarios
 * @property Alumateria[] $alumaterias
 */
class Alucarrera extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alucarrera';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Carrera',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAluhorarios()
    {
        return $this->hasMany(Aluhorario::className(), ['idcarrera' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlumaterias()
    {
        return $this->hasMany(Alumateria::className(), ['idcarrera' => 'id']);
    }
}
