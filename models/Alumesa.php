<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumesa".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property Aluexamen[] $aluexamens
 */
class Alumesa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alumesa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Llamado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAluexamens()
    {
        return $this->hasMany(Aluexamen::className(), ['idmesa' => 'id']);
    }
}
