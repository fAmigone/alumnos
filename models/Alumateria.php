<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumateria".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $idcarrera
 *
 * @property Aluhorario[] $aluhorarios
 * @property Alucarrera $idcarrera0
 */
class Alumateria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alumateria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'idcarrera'], 'required'],
            [['idcarrera'], 'integer'],
            [['nombre'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Materia',
            'idcarrera' => 'Carrera',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAluhorarios()
    {
        return $this->hasMany(Aluhorario::className(), ['idmateria' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdcarrera0()
    {
        return $this->hasOne(Alucarrera::className(), ['id' => 'idcarrera']);
    }
}
