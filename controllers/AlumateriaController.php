<?php

namespace app\controllers;

use Yii;
use app\models\Alumateria;
use app\models\AlumateriaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AlumateriaController implements the CRUD actions for Alumateria model.
 */
class AlumateriaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Alumateria models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AlumateriaSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Alumateria model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchAluhorariodetalleModel = new \app\models\AluhorariodetalleSearch;          
        $dataAluhorariodetalleProvider = $searchAluhorariodetalleModel->search(['AluhorariodetalleSearch'=>['idmateria'=>$id]]);
        //$modelHorarioDetalle = $this->crearDetalle($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataAluhorariodetalleProvider' => $dataAluhorariodetalleProvider,
            'searchAluhorariodetalleModel' => $searchAluhorariodetalleModel,
        ]);
    }
      protected function crearDetalle($id) {
        $model = new \app\models\Aluhorariodetalle;
        $model->idmateria = $id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //flash
        }
        return $model;
    }
    /**
     * Creates a new Alumateria model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Alumateria;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Alumateria model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Alumateria model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Alumateria model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Alumateria the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Alumateria::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
