<?php

namespace app\controllers;

use Yii;
use app\models\Aluhorario;
use app\models\AluhorarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AluhorarioController implements the CRUD actions for Aluhorario model.
 */
class AluhorarioController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Aluhorario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AluhorarioSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        //$searchModel = new AluhorariodetalleSearch;
        //$dataProvider = $searchExpositorModel->search(['AluhorariodetalleSearch'=>['idhorario'=>$id]]);
        

        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Aluhorario model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)            
    {   $searchModel = new AluhorarioSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        
        //instanciamos un dataprovider que contenga todos los horarios de la materia
        $searchAluhorariodetalleModel = new \app\models\AluhorariodetalleSearch;          
        $dataAluhorariodetalleProvider = $searchAluhorariodetalleModel->search(['AluhorariodetalleSearch'=>['idaluhorario'=>$id]]);
        $modelHorarioDetalle = $this->crearDetalle($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
                    'modelUnDetalle' => $modelHorarioDetalle,
                    'dataAluhorariodetalleProvider' => $dataAluhorariodetalleProvider,
                    'searchAluhorariodetalleModel' => $searchAluhorariodetalleModel,
                    'searchModel' => $searchModel,
                    
            
        ]);
    }
       protected function crearDetalle($id) {
        $model = new \app\models\Aluhorariodetalle;
        $model->idaluhorario = $id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //flash
        }
        return $model;
    }
    /**
     * Creates a new Aluhorario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Aluhorario;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Aluhorario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Aluhorario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Aluhorario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Aluhorario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Aluhorario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionLists($id)
    {

        $countMaterias = Aluhorario::find()
                ->where(['idcarrera' => $id])
                ->count();
 
        $materias = Aluhorario::find()
                ->where(['idcarrera' => $id])
                ->orderBy('id DESC')
                ->all();
 
        if($countMaterias>0){            
            foreach($materias as $materia){                                
                echo "<option value='".$materia->id."'>".$materia->idmateria."</option>";
            }
        }
        else{
            
            echo "<option>-</option>";
        }
 
    }
    
}
